/*Coding question:-
A Array of 0s and 1s
WAP to put all 1s on left of array and 0s to right

Example:-
NumArray=[1,0,1,1,0,0,1]
Output required
NumArray=[1,1,1,1,0,0,0]*/

package CommitPushPullGit;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SwapNumArray {

	public static void main(String[] args) {
		/*//Create a mutable(unchangeable) list of Integer array using Arrays.asList()
        List<Integer> list = Arrays.asList(1,0,1,1,0,0,1); 
        // create a ArrayList of integer arrays 
		List<integer> list = new ArrayList<>(Arrays.asList(1,0,1,1,0,0,1));
       */       
		//creating integer type arrays
        Integer numArray[] = {1,0,1,1,0,0,1}; 

        // getting the list view of Array 
        List<Integer> list = Arrays.asList(numArray); 
        
        System.out.println(list);
         
        // swap the 1st and 6th elements 
        Collections.swap(list, 1, 6);
         
        System.out.println(list);
	}

}
